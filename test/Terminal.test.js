import Terminal from '../src/Terminal.js';
import { expect } from 'chai';

describe( 'Terminal', () => {
	it( 'is a class', () => {
		expect( Terminal ).to.be.a( 'function' );
	} );
} );
